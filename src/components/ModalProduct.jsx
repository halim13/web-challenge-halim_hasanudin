import { Fragment, useEffect, useRef, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import Carousel from './carousel/Carousel'

const TextInfo = ({ label, description }) => <div className='mb-2 flex-1'>
    <p className='text-sm'>{label}</p>
    <p className="text-sm text-gray-500">
        {description}
    </p>
</div>

export default function ModalProduct(props) {
    const { show, data, setShow } = props
    const [open, setOpen] = useState(!!show)

    useEffect(() => {
        setOpen(!!show)

        return () => null
    }, [show])

    const closeModal = () => {
        setOpen(false)
        if (setShow) {
            setShow(false)
        }
    }

    const cancelButtonRef = useRef(null)

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog as="div" className="relative z-10" initialFocus={cancelButtonRef} onClose={closeModal}>
                <Transition.Child
                    as={Fragment}
                    enter="ease-out duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                </Transition.Child>

                <div className="fixed inset-0 z-10 overflow-y-auto">
                    <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                        <Transition.Child
                            as={Fragment}
                            enter="ease-out duration-300"
                            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                            enterTo="opacity-100 translate-y-0 sm:scale-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        >
                            <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg">
                                <div className="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
                                    <div className="sm:flex sm:items-start">
                                        <div className="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left flex-1">
                                            <Dialog.Title as="h2" className="text-base font-semibold leading-6 text-gray-900">
                                                {data?.title}
                                            </Dialog.Title>
                                            <button onClick={() => closeModal()} type="button" className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white" data-modal-hide="authentication-modal">
                                                <svg aria-hidden="true" className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                                                <span className="sr-only">Close modal</span>
                                            </button>
                                            <div className="mt-2">
                                                <Carousel
                                                    styleClass={`my-3 mx-auto w-2/3 h-64 rounded-md `}
                                                    style={{ height: '250px', width: '100%' }}
                                                    timespace={2500}
                                                    arrowVisible={true}
                                                    hideArrowForMobile={true}
                                                    pageIndicator={true}
                                                    images={data?.images}
                                                />
                                                <TextInfo
                                                    label={'Description'}
                                                    description={data?.description}
                                                />
                                                <div className='flex'>
                                                    <TextInfo
                                                        label={'Stock'}
                                                        description={data?.stock}
                                                    />
                                                    <TextInfo
                                                        label={'Price'}
                                                        description={'$' + data?.price}
                                                    />
                                                </div>
                                                <div className='flex'>
                                                    <TextInfo
                                                        label={'Discount'}
                                                        description={data?.discountPercentage + '%'}
                                                    />
                                                    <TextInfo
                                                        label={'Rating'}
                                                        description={data?.rating}
                                                    />
                                                </div>
                                                <div className='flex'>
                                                    <TextInfo
                                                        label={'Brand'}
                                                        description={data?.brand}
                                                    />
                                                    <TextInfo
                                                        label={'Category'}
                                                        description={data?.category}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Dialog.Panel>
                        </Transition.Child>
                    </div>
                </div>
            </Dialog>
        </Transition.Root>
    )
}

import { parseParamToString } from './commonUtils'

export const requestGet = async (url, params, isRefreshed = false) => {
    try {
        let requestOptions = {
            method: 'GET',
        }

        return fetch(`${url}${parseParamToString(params)}`, requestOptions)
            .then(response => response.json())
            .catch(error =>
                Promise.reject(error)
            )
    } catch (err) {
        if (!isRefreshed) {
            return requestGet(url, params, true)
        } else return Promise.reject(err)
    }
}

export const requestPost = async (url, body) => {
    try {
        let requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body),
        }

        const response = await fetch(`${url}`, requestOptions)
        const responseText = await response.text()
        if (response.status === 200) {
            return JSON.parse(responseText || '{}')
        } else {
            return Promise.reject(JSON.parse(responseText || '{}'))
        }
    } catch (err) {
        return Promise.reject(err)
    }
}
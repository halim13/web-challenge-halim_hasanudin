import React, { useState } from 'react'
import { Outlet, useLocation, useNavigate } from 'react-router-dom'
import Navbar from '../components/Navbar'
import Sidebar from '../components/Sidebar'
import navigation from '../constants/navigation'
import { useSelector } from 'react-redux'

export default function Dashboard() {
    const location = useLocation()
    const [sidebarNav, setSidebarNav] = useState(location.pathname ? navigation[location.pathname] : 'dashboard')
    const user = useSelector(({ user }) => user.profile)
    const navigate = useNavigate()

    if (!user?.token) {
        navigate('/login')
    }
    return (
        <div>
            <Sidebar nav={sidebarNav} setSidebar={setSidebarNav} />
            <div className="relative md:ml-64 bg-blueGray-100">
                <Navbar nav={sidebarNav} />
                <div className="relative bg-purple-600 md:pt-20" />
                <Outlet />
            </div>
        </div>
    )
}

import React, { useEffect } from 'react'
import { useNavigate, Outlet, useLocation } from 'react-router-dom'
import Modal from '../components/Modal'
import Loading from '../components/Loading'
import { useJwt } from 'react-jwt'
import { useDispatch, useSelector } from 'react-redux'
import { setUser } from '../app-redux/slicer/userSlice'

export default function Main() {
    const navigate = useNavigate()
    const location = useLocation()
    const dispatch = useDispatch()

    const user = useSelector(({ user }) => user.profile)
    const jwt = useJwt(user?.token || '')

    useEffect(() => {
        checkLogin()
        return () => null
    }, [location.pathname])

    const checkLogin = () => {
        let isLogin = false

        if (user?.token) {
            if (!!jwt?.isExpired) {
                dispatch(setUser(null))
            } else {
                isLogin = true
            }
        }

        if (isLogin) {
            navigate(location.pathname?.includes('dashboard') ? location.pathname : '/dashboard')
        } else {
            if (location.pathname === '/') {
                navigate('/login')
            } else {
                navigate(location.pathname)
            }
        }
    }

    return (
        <div className='relative'>
            <Loading />
            <Outlet />
            <Modal />
        </div>
    )
}

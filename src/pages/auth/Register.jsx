import React from 'react'
import { Link } from 'react-router-dom'
import Logo from '../../assets/logo.png'
import HeaderLogin from '../../assets/header-login.png'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { setLoading } from '../../app-redux/slicer/loadingSlice'
import { setModal } from '../../app-redux/slicer/modalSlice'
import { requestPost } from '../../utils/requestUtils'
import { BASE_URL } from '../../constants/common'
import errorMessage from '../../constants/errorMessage'

function Register() {
    const {
        register,
        handleSubmit,
    } = useForm()
    const dispatch = useDispatch()

    const onSubmit = async (data) => {
        try {
            dispatch(setLoading(true))
            if (
                !data.firstName
                || !data.lastName
                || !data.email
                || !data.userid
                || !data.password
                || !data.repassword
            ) {
                dispatch(setLoading(false))
                dispatch(setModal({
                    show: true,
                    type: 'error',
                    label: 'Register Error',
                    message: 'Harap isi semua inputan!',
                }))
            } else {
                if (data.password !== data.repassword) {
                    dispatch(setModal({
                        show: true,
                        type: 'error',
                        label: 'Password tidak sama',
                        message: 'Password dan Re-password harus sama!',
                    }))
                    dispatch(setLoading(false))
                } else {
                    const response = await requestPost(BASE_URL + '/users/add', ({
                        username: data.userid,
                        password: data.password,
                        firstName: data.firstName,
                        lastName: data.lastName,
                        email: data.email,
                    }))
                    dispatch(setLoading(false))
                    dispatch(setModal({
                        show: true,
                        type: 'success',
                        label: 'Register Success',
                        message: 'Successfully register, but you cannot login!',
                    }))
                }
            }
        } catch (error) {
            const { message } = error

            dispatch(setLoading(false))
            dispatch(setModal({
                show: true,
                type: 'error',
                label: 'Register Error',
                message: errorMessage[message] || error.message || '',
            }))
        }
    }

    return (
        <div className='p-8'>
            <div className=' max-w-lg mx-auto relative bg-white'>
                <img
                    className='w-24 absolute'
                    src={HeaderLogin}
                    alt='Header login'
                />
                <div className='max-w-xl px-6 py-8'>
                    <div className='sm:mx-auto sm:w-full sm:max-w-sm'>
                        <img
                            className='mx-auto w-40'
                            src={Logo}
                            alt='Logo'
                        />
                        <h2 className='mt-5 text-2xl font-bold leading-9 tracking-tight text-gray-900'>
                            Register
                        </h2>
                        <p className=' leading-9 tracking-tight text-gray-900'>
                            Register an account.
                        </p>
                    </div>

                    <div className='mt-5 sm:mx-auto sm:w-full sm:max-w-sm'>
                        <form className='space-y-6' onSubmit={handleSubmit(onSubmit)}>
                            <div className='border-b border-purple-800'>
                                <label htmlFor='firstName' className='block text-sm font-medium leading-6 text-gray-900'>
                                    First Name
                                </label>
                                <input
                                    id='firstName'
                                    name='firstName'
                                    type='text'
                                    placeholder='First Name'
                                    // required
                                    className='block w-full border-none py-1.5 text-gray-900 placeholder:text-gray-400 focus:outline-none appearance-none bg-transparent sm:text-sm sm:leading-6'
                                    {...register('firstName')}
                                />
                            </div>
                            <div className='border-b border-purple-800'>
                                <label htmlFor='lastName' className='block text-sm font-medium leading-6 text-gray-900'>
                                    Last Name
                                </label>
                                <input
                                    id='lastName'
                                    name='lastName'
                                    type='text'
                                    placeholder='Last Name'
                                    // required
                                    className='block w-full border-none py-1.5 text-gray-900 placeholder:text-gray-400 focus:outline-none appearance-none bg-transparent sm:text-sm sm:leading-6'
                                    {...register('lastName')}
                                />
                            </div>
                            <div className='border-b border-purple-800'>
                                <label htmlFor='email' className='block text-sm font-medium leading-6 text-gray-900'>
                                    Email
                                </label>
                                <input
                                    id='email'
                                    name='email'
                                    type='email'
                                    placeholder='Email'
                                    // required
                                    className='block w-full border-none py-1.5 text-gray-900 placeholder:text-gray-400 focus:outline-none appearance-none bg-transparent sm:text-sm sm:leading-6'
                                    {...register('email')}
                                />
                            </div>
                            <div className='border-b border-purple-800'>
                                <label htmlFor='user-id' className='block text-sm font-medium leading-6 text-gray-900'>
                                    Username
                                </label>
                                <input
                                    id='user-id'
                                    name='user-id'
                                    type='text'
                                    placeholder='Username'
                                    // required
                                    className='block w-full border-none py-1.5 text-gray-900 placeholder:text-gray-400 focus:outline-none appearance-none bg-transparent sm:text-sm sm:leading-6'
                                    {...register('userid')}
                                />
                            </div>

                            <div className='border-b border-purple-800'>
                                <div className='flex items-center justify-between'>
                                    <label htmlFor='password' className='block text-sm font-medium leading-6 text-gray-900'>
                                        Password
                                    </label>
                                </div>
                                <input
                                    id='password'
                                    name='password'
                                    type='password'
                                    // required
                                    placeholder='Password'
                                    className='block w-full border-none py-1.5 text-gray-900 placeholder:text-gray-400 focus:outline-none appearance-none bg-transparent sm:text-sm sm:leading-6'
                                    {...register('password')}
                                />
                            </div>
                            <div className='border-b border-purple-800'>
                                <div className='flex items-center justify-between'>
                                    <label htmlFor='repassword' className='block text-sm font-medium leading-6 text-gray-900'>
                                        Re-Password
                                    </label>
                                </div>
                                <input
                                    id='repassword'
                                    name='repassword'
                                    type='password'
                                    // required
                                    placeholder='re-password'
                                    className='block w-full border-none py-1.5 text-gray-900 placeholder:text-gray-400 focus:outline-none appearance-none bg-transparent sm:text-sm sm:leading-6'
                                    {...register('repassword')}
                                />
                            </div>
                            <div className='flex justify-end'>
                                <button
                                    type='submit'
                                    className='flex justify-center rounded-full bg-purple-800 px-10 py-2 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-purple-600'
                                >
                                    REGISTER
                                </button>
                            </div>
                        </form>

                        <p className='mt-5 text-center text-sm text-gray-500'>
                            Have an account?{' '}
                            <Link to='/login' className='font-semibold leading-6 text-orange-600 hover:text-orange-500'>
                                Login
                            </Link>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Register
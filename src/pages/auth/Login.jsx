import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Logo from '../../assets/logo.png'
import HeaderLogin from '../../assets/header-login.png'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { setLoading } from '../../app-redux/slicer/loadingSlice'
import { setModal } from '../../app-redux/slicer/modalSlice'
import { requestPost } from '../../utils/requestUtils'
import { BASE_URL } from '../../constants/common'
import { setUser } from '../../app-redux/slicer/userSlice'
import errorMessage from '../../constants/errorMessage'

function Login() {
    const {
        register,
        handleSubmit,
    } = useForm()
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const onSubmit = async (data) => {
        try {
            dispatch(setLoading(true))
            if (!data.userid || !data.password) {
                dispatch(setLoading(false))
                dispatch(setModal({
                    show: true,
                    type: 'error',
                    label: 'Login Error',
                    message: 'User ID atau password anda belum diisi!',
                }))
            } else {
                const response = await requestPost(BASE_URL + '/auth/login', ({
                    username: data.userid,
                    password: data.password,
                }))
                dispatch(setUser(response))
                dispatch(setLoading(false))
                navigate('/dashboard')
            }
        } catch (error) {
            const { message } = error

            dispatch(setLoading(false))
            dispatch(setModal({
                show: true,
                type: 'error',
                label: 'Login Error',
                message: errorMessage[message] || error.message || '',
            }))
        }
    }

    return (
        <div className='p-8'>
            <div className=' max-w-lg mx-auto relative bg-white'>
                <img
                    className='w-24 absolute'
                    src={HeaderLogin}
                    alt='Header login'
                />
                <div className='max-w-xl px-6 py-8'>
                    <div className='sm:mx-auto sm:w-full sm:max-w-sm'>
                        <img
                            className='mx-auto w-40'
                            src={Logo}
                            alt='Logo'
                        />
                        <h2 className='mt-5 text-2xl font-bold leading-9 tracking-tight text-gray-900'>
                            Login
                        </h2>
                        <p className=' leading-9 tracking-tight text-gray-900'>
                            Please sign in to continue.
                        </p>
                    </div>

                    <div className='mt-5 sm:mx-auto sm:w-full sm:max-w-sm'>
                        <form className='space-y-6' onSubmit={handleSubmit(onSubmit)}>
                            <div className='border-b border-purple-800'>
                                <label htmlFor='user-id' className='block text-sm font-medium leading-6 text-gray-900'>
                                    User ID
                                </label>
                                <input
                                    id='user-id'
                                    name='user-id'
                                    type='text'
                                    autoComplete='user-id'
                                    placeholder='User ID'
                                    // required
                                    className='block w-full border-none py-1.5 text-gray-900 placeholder:text-gray-400 focus:outline-none appearance-none bg-transparent sm:text-sm sm:leading-6'
                                    {...register('userid')}
                                />
                            </div>

                            <div className='border-b border-purple-800'>
                                <div className='flex items-center justify-between'>
                                    <label htmlFor='password' className='block text-sm font-medium leading-6 text-gray-900'>
                                        Password
                                    </label>
                                </div>
                                <input
                                    id='password'
                                    name='password'
                                    type='password'
                                    autoComplete='current-password'
                                    // required
                                    placeholder='Password'
                                    className='block w-full border-none py-1.5 text-gray-900 placeholder:text-gray-400 focus:outline-none appearance-none bg-transparent sm:text-sm sm:leading-6'
                                    {...register('password')}
                                />
                            </div>
                            <div className='flex justify-end'>
                                <button
                                    type='submit'
                                    className='flex justify-center rounded-full bg-purple-800 px-10 py-2 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-purple-600'
                                >
                                    LOGIN
                                </button>
                            </div>
                        </form>

                        <p className='mt-5 text-center text-sm text-gray-500'>
                            Dont have an account?{' '}
                            <Link to='/register' className='font-semibold leading-6 text-orange-600 hover:text-orange-500'>
                                Register
                            </Link>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login
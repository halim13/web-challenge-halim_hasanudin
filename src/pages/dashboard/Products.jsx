import React, { useEffect, useState } from 'react'
import { requestGet } from '../../utils/requestUtils'
import { BASE_URL } from '../../constants/common'
import { useDispatch } from 'react-redux'
import { setLoading } from '../../app-redux/slicer/loadingSlice'
import { setModal } from '../../app-redux/slicer/modalSlice'
import { convertCurrency } from '../../utils/commonUtils'
import ModalProduct from '../../components/ModalProduct'
import Dropdown from '../../components/Dropdown'

function Products() {
    const [products, setProducts] = useState([])
    const [selectedItem, setSelectedItem] = useState(null)

    const [page, setPage] = useState({
        limit: 10,
        size: 0,
        current: 1,
        numberOfPages: 1,
        min: 0,
        max: 10,
    })
    const [search, setSearch] = useState('')
    const [showModal, setShowModal] = useState(false)

    const dispatch = useDispatch()

    useEffect(() => {
        fetchData()
        return () => null
    }, [])

    useEffect(() => {
        setCurrentPage(0)
        return () => null
    }, [page.limit])

    const fetchData = async () => {
        try {
            dispatch(setLoading(true))
            const result = await requestGet(BASE_URL + '/products', {
                limit: 0,
            })
            setProducts(result.products)
            setCurrentPage(0, result.products)
            dispatch(setLoading(false))
        } catch (error) {
            dispatch(setLoading(false))
            dispatch(setModal({
                show: true,
                type: 'error',
                label: 'Error fetch data products',
                message: error?.message || error
            }))
        }
    }

    const setCurrentPage = async (curr, prod) => {
        const data = prod || products
        const numberOfPages = Math.ceil(data.length / page.limit)
        const minSize = (((curr || page.current) - 1) * page.limit) - 1
        const maxSize = minSize + page.limit
        const dataLength = data.length

        setPage(p => ({
            ...p,
            current: curr ? curr : p.current,
            size: dataLength || 0,
            numberOfPages,
            min: !!dataLength ? minSize + 1 : 0,
            max: maxSize > dataLength ? dataLength : maxSize + 1,
        }))
        return ({ minSize, maxSize })
    }

    const nextPage = async () => {
        await setCurrentPage(page.current + 1)
    }
    const prevPage = async () => {
        const p = page.current === 0 ? 0 : page.current - 1
        await setCurrentPage(p)
    }
    const setPages = async (p) => {
        await setCurrentPage(p)
    }

    const filterProducts = products
        .filter(product =>
            product.title.toLowerCase().includes(search)
            || product.price.toString().toLowerCase().includes(search)
            || product.stock.toString().toLowerCase().includes(search)
            || product.rating.toString().toLowerCase().includes(search)
        )
        .slice(page.min, page.max)

    return (
        <div className="px-4 md:px-10 mx-auto w-full">
            <div className="flex flex-wrap mt-4">
                <div className="w-full mb-12 xl:mb-0 px-4">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
                        <div className="rounded-t mb-0 px-4 py-3 border-0 flex justify-between items-center">
                            <div className="relative mt-1">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg className="w-5 h-5 text-gray-500 dark:text-gray-400" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clipRule="evenodd"></path></svg>
                                </div>
                                <input
                                    type="text"
                                    id="table-search"
                                    className="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Search for items"
                                    value={search}
                                    onChange={e => setSearch(e.target.value)}
                                />
                            </div>
                            <Dropdown limit={page.limit} setLimit={setPage} />
                        </div>
                        <div className="block w-full overflow-x-auto">
                            {/* Projects table */}
                            <table className="items-center w-full bg-transparent border-collapse">
                                <thead>
                                    <tr>
                                        <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                            Thumbnail
                                        </th>
                                        <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                            Name
                                        </th>
                                        <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                            Price
                                        </th>
                                        <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                            Stock
                                        </th>
                                        <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                            Rating
                                        </th>
                                        <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        filterProducts.map((product, i) => <tr key={i} className='dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600'>
                                            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 w-24">
                                                <img src={product.thumbnail} alt={product.title} />
                                            </td>
                                            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 w-24">
                                                {product.title}
                                            </td>
                                            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                {convertCurrency(product.price)}
                                            </td>
                                            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                {product.stock}
                                            </td>
                                            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                {product.rating}
                                            </td>
                                            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                <a
                                                    href='#'
                                                    className="font-medium text-blue-600 dark:text-blue-500 hover:underline"
                                                    onClick={e => {
                                                        e.preventDefault()
                                                        setSelectedItem(product)
                                                        setShowModal(true)
                                                    }}
                                                >View</a>
                                            </td>
                                        </tr>)
                                    }

                                </tbody>
                            </table>
                            <hr />
                            <nav className="flex items-center justify-between p-4" aria-label="Table navigation">
                                <span className="text-sm font-normal text-gray-500 dark:text-gray-400">Showing <span className="font-semibold text-gray-900 dark:text-white">
                                    {`${!!filterProducts.length ? page.min + 1 : 0}-${filterProducts.length * page.current}`}
                                </span> of <span className="font-semibold text-gray-900 dark:text-white">{products.length}</span></span>
                                <ul className="inline-flex items-center -space-x-px">
                                    <li>
                                        <a
                                            href='#'
                                            onClick={(e) => {
                                                e.preventDefault()
                                                prevPage()
                                            }}
                                            className="block px-3 py-2 ml-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-l-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
                                        >
                                            <span className="sr-only">Previous</span>
                                            <svg className="w-5 h-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clipRule="evenodd"></path></svg>
                                        </a>
                                    </li>
                                    {
                                        !!page.numberOfPages && [...Array(page.numberOfPages).keys()].map(map => <li key={map}>
                                            <a
                                                href='#'
                                                onClick={(e) => {
                                                    e.preventDefault()
                                                    setPages(map + 1)
                                                }}
                                                className={`${page.current === map + 1 ? 'text-blue-600 bg-blue-50 hover:bg-blue-100 hover:text-blue-700  dark:bg-gray-700 dark:text-white' : ''} px-3 py-2 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white`}
                                            >{map + 1}</a>
                                        </li>
                                        )}
                                    <li>
                                        <a
                                            href='#'
                                            onClick={e => {
                                                e.preventDefault()
                                                page.current >= page.numberOfPages ? {} : nextPage()
                                            }}
                                            className="block px-3 py-2 leading-tight text-gray-500 bg-white border border-gray-300 rounded-r-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
                                        >
                                            <span className="sr-only">Next</span>
                                            <svg className="w-5 h-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd"></path></svg>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <ModalProduct data={selectedItem} show={showModal} setShow={(val) => setShowModal(val)} />
        </div>
    )
}

export default Products
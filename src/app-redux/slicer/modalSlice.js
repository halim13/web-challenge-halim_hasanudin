import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    show: false,
    type: 'success',
    label: '',
    message: '',
}

export const modalSlice = createSlice({
    name: 'modal',
    initialState,
    reducers: {
        setModal: (state, action) => {
            state.show = !!action.payload.show || !!initialState.show
            state.type = action.payload.type || state.type
            state.label = action.payload.label || state.label
            state.message = action.payload.message || state.message
        },
    },
})

export const { setModal } = modalSlice.actions

export default modalSlice.reducer
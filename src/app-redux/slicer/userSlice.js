import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    profile: {},
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setUser: (state, action) => {
            state.profile = action.payload
        },
    },
})

export const { setUser } = userSlice.actions

export default userSlice.reducer
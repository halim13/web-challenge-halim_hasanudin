import { combineReducers, configureStore } from '@reduxjs/toolkit'
import loadingReducer from './slicer/loadingSlice'
import modalReducer from './slicer/modalSlice'
import userReducer from './slicer/userSlice'
import storage from 'redux-persist/lib/storage';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk'

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['user'],
}

const rootReducer = combineReducers({
    loading: loadingReducer,
    modal: modalReducer,
    user: userReducer,
})
const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
    reducer: persistedReducer,
    middleware: [thunk]
})

export const persistor = persistStore(store)

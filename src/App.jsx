import './App.css'
import { Route, createBrowserRouter, createRoutesFromElements, RouterProvider } from 'react-router-dom'
import Login from './pages/auth/Login'
import Dashboard from './pages/Dashboard'
import Register from './pages/auth/Register'
import Main from './pages/Main'
import Products from './pages/dashboard/Products'
import MainDashboard from './pages/dashboard/MainDashboard'
import Error from './pages/Error'

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path='/' element={<Main />} errorElement={<Error />}>
      <Route path='login' element={<Login />} />
      <Route path='register' element={<Register />} />
      <Route path='dashboard' element={<Dashboard />}>
        <Route index element={<MainDashboard />} />
        <Route path='products' element={<Products />} />
      </Route>
    </Route>
  )
)

const App = ({ routes }) => (
  <RouterProvider router={router} />
)

export default App

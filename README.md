## Web Challenge
This is technical test from PT. Infosys Solusi Terpadu

## Built With
[![Vite](https://img.shields.io/badge/ViteJS-4.3.9-blue.svg?style=rounded-square)](https://vitejs.dev/)

## How to run the project ?
To run project, follow these steps:
```
- open your terminal
- git clone https://gitlab.com/halim13/web-challenge-halim_hasanudin.git
- cd web-challenge-halim_hasanudin
- npm install
- npm run dev
- open http://127.0.0.1:5173 in browser
```

## Deploy Application
[Vercel](https://web-challenge-halim-hasanudin.vercel.app/)

## Screenshoot
![login](screenshoot/ss1.png)
![register](screenshoot/ss5.png)
![products](screenshoot/ss3.png)
![products item](screenshoot/ss4.png)